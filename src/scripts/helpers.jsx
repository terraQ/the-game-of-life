const deepCopyArray = (array) => JSON.parse(JSON.stringify(array));

export default deepCopyArray;
