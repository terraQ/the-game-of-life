import deepCopyArray from '../../scripts/helpers';
import styles from './Grid.module.scss';

const Grid = ({ disabled, grid, setGrid }) => {
  const handleClick = (rowIndex, columnIndex) => {
    let newGrid = deepCopyArray(grid);

    newGrid[rowIndex][columnIndex] = grid[rowIndex][columnIndex] ? 0 : 1;

    setGrid(newGrid);
  };

  return (
    <div className={styles.grid}>
      {grid.map((row, rowIndex) => {
        return (
          <div className={styles.row}>
            {row.map((cellValue, columnIndex) => (
              <button
                className={`${styles.cell} ${
                  grid[rowIndex][columnIndex] ? styles.isAlive : styles.isDead
                }`}
                disabled={disabled}
                key={`${rowIndex}${columnIndex}`}
                onClick={() => handleClick(rowIndex, columnIndex)}
                value={cellValue}
              />
            ))}
          </div>
        );
      })}
    </div>
  );
};

export default Grid;
