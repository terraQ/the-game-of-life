import styles from './Button.module.scss';

const Button = ({ children, disabled, onClick, type }) => {
  return (
    <button
      type="button"
      onClick={onClick}
      className={`${styles.button} ${type ? styles[type] : ''}`}
      disabled={disabled}
    >
      {children}
    </button>
  )
};

export default Button;
