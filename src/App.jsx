import { useEffect, useState } from 'react';
import deepCopyArray from './scripts/helpers';
import Button from './components/Button/Button';
import Grid from './components/Grid/Grid';
import styles from './App.module.scss';

const NUMBER_OF_COLUMNS = 30;
const NUMBER_OF_ROWS = 30;

const OPERATIONS = [
  [-1, -1],
  [0, -1],
  [1, -1],
  [-1, 0],
  [1, 0],
  [-1, 1],
  [0, 1],
  [1, 1],
];

const createEmptyGrid = () => {
  const rows = [];
  const cells = Array.from(Array(NUMBER_OF_COLUMNS), () => 0);

  for (let i = 0; i < NUMBER_OF_ROWS; i++) {
    rows.push(cells);
  }

  return rows;
};

function App() {
  const [grid, setGrid] = useState(createEmptyGrid);
  const [play, setPlay] = useState(false);
  const [populationSize, setPopulationSize] = useState(0);
  const [initialState, setInitialState] = useState();
  let [generationNumber, setGenerationNumber] = useState(0);

  useEffect(() => {
    if (generationNumber === 0) setInitialState(grid);
    countPopulation();
  }, [grid]);

  useEffect(() => {
    if (play) {
      const timer = setInterval(nextGeneration, 1000);
      return () => {
        clearInterval(timer);
      };
    }
  }, [play, grid]);

  const createRandomGrid = () => {
    const rows = [];

    for (let i = 0; i < NUMBER_OF_ROWS; i++) {
      rows.push(
        Array.from(Array(NUMBER_OF_COLUMNS), () =>
          Math.random() > 0.5 ? 1 : 0
        )
      );
    }

    setGrid(rows);
  };

  const clearGrid = () => {
    setGenerationNumber(0);
    setGrid(createEmptyGrid);
  };

  const resetGrid = () => {
    setGrid(initialState);
    setGenerationNumber(0);
  };

  const nextGeneration = () => {
    let newGrid = deepCopyArray(grid);

    for (let rowIndex = 0; rowIndex < NUMBER_OF_ROWS; rowIndex++) {
      for (let colIndex = 0; colIndex < NUMBER_OF_COLUMNS; colIndex++) {
        let neighbours = 0;

        OPERATIONS.forEach(([x, y]) => {
          const adjacentXCoordinate = rowIndex + x;
          const adjacentYCoordinate = colIndex + y;

          if (
            adjacentXCoordinate >= 0 &&
            adjacentXCoordinate < NUMBER_OF_ROWS &&
            adjacentYCoordinate >= 0 &&
            adjacentYCoordinate < NUMBER_OF_COLUMNS
          ) {
            neighbours += grid[adjacentXCoordinate][adjacentYCoordinate];
          }
        });

        if (neighbours < 2 || neighbours > 3) {
          newGrid[rowIndex][colIndex] = 0;
        } else if (grid[rowIndex][colIndex] === 0 && neighbours === 3) {
          newGrid[rowIndex][colIndex] = 1;
        }
      }
    }

    setGenerationNumber(++generationNumber);
    setGrid(newGrid);
  };

  const countPopulation = () => {
    let population = 0;

    for (let rowIndex = 0; rowIndex < NUMBER_OF_ROWS; rowIndex++) {
      for (let colIndex = 0; colIndex < NUMBER_OF_COLUMNS; colIndex++) {
        if (grid[rowIndex][colIndex] === 1) population++;
      }
    }
    setPopulationSize(population);
  };

  return (
    <div className={styles.app}>
      <div className={styles.row}>
        <h1>Conway's Game of Life</h1>
      </div>
      <div className={styles.row}>
        <Button onClick={() => setPlay(!play)}>
          {play ? 'Pause' : 'Play'}
        </Button>
      </div>
      <div className={styles.row}>
        <div className={styles.col}>
          <Grid
            disabled={play || generationNumber > 0 || false}
            grid={grid}
            setGrid={setGrid}
          />
        </div>
        <div className={styles.col}>
          <Button
            disabled={play || generationNumber > 0}
            onClick={createRandomGrid}
            type='transparent'
          >
            Random
          </Button>
          <Button disabled={play} onClick={nextGeneration} type='transparent'>
            Step Forward
          </Button>
          <Button disabled={play} onClick={clearGrid} type='transparent'>
            Clear
          </Button>
          <Button disabled={play} onClick={resetGrid} type='transparent'>
            Reset
          </Button>
          <h3>Generation Number: {generationNumber}</h3>
          <h3>Size of the Population: {populationSize}</h3>
        </div>
      </div>
    </div>
  );
}

export default App;
